console.log("Hello World");
/*
	FUNCTIONS:
			lines/blocks of codes that tell our device/application to perform certain tasks when called/invoked.

			functions are mostly created to create complicated tasks to run several lines for codes in succession.

			they are used to prevent repeating lines/blocks of codes that perform the same task/function.
*/

// function declaration
/*
	function - creates/defines a function in javascript indicator that we are creating a function that will be invoked later in the code.
	printName() - function name; functions are named to be able to use later in the code.
	function block {} - statements which will comprise of the body of the function. This is where the codes are to be executed.

	default syntax: 
		function functionName(){
			function statements/code block
		}
*/
function printName(){
	console.log("My name is John");
};

// call/invoke a function
/*
	the code block and statements inside a function is not immediately executed when the function is defined/declared. the code block/statements inside a fxn is executed when invoked/called

	it is common to use the term (call a function) instead of "invoke a fxn".
*/
printName();

// declaredFunction();
// results in an error due to the non-existence of the function (fxn not declared)

// DECLARATION and EXPRESSION
// declaration - using function keyword and functionName
declaredFunction();
/*
	functions in JS can be used before it can be defined, thus hoisting is allowed 	for JS fxns.
		NOTE: hoisting in JS's default behavior for certain variable and functions to run/use them before their declaration..
*/
function declaredFunction() {
	console.log("Hello from declaredFunction");
}


// FUNCTION EXPRESSION	
/*
	a function can also be created by storing it inside a variable.

	does not allow hoisting.

	a function expression is an anonymous function that is assigned to the variableFunction.
		anonymous function - unnamed function
*/

// variableFunction();
	// calling a Function Expression before declaration will result in an error.



let variableFunction = function(){
	console.log("Hello again!");
};

variableFunction();

/*
	miniactivity

	create 2 functions 
		1st function
			log in console the top 3 anime you would like to recommend
		2nd function
			log in console top 3 movies you would like to recommend
*/
let animeList = ["Jujutsu Kaisen", "SpyXFamily", "86"];
let movieList = ["Avengers: End Game", "Multiverse of Madness", "Spiderman: Far From Home"]

function topAnime() {
	console.log("My top 3 anime are: ");
	console.log(animeList);
}

function topMovies() {
	console.log("My top 3 movies are: ");
	console.log(movieList);
}

topAnime();
topMovies();



// Reassigning Declared functions

declaredFunction = function(){
	console.log("Updated declaredFunction");
}

declaredFunction();

// However, we cannot change the declared functions/ function expressions that are defined using CONST 
const constFunction = function(){
	console.log("Initialized constFunction");
};
constFunction();
/*
constFunction = function(){
	console.log("cannot be re-assigned.");
};
*/

// Function Scoping
/*
	scope is the accessibility of variables/functions
*/

{
	let localVar = "Armando Perez";
	console.log(localVar);

}

let globalVar = "Mr. Worldwide";

console.log(globalVar);

// JS variables/functions have 3 scopes
	/*
		LOCAL/Block scope - can be accessed inside {}/code block

		GLOBAL scope - can be accessed anywhere in the js file
	
		FUNCTION scope - 	
	*/

function showNames(){
	var functionVar = "Joe";
	const functionConst = "John";
	let functionLet = "Jane";

	console.log(functionVar);
	console.log(functionConst);
	console.log(functionLet);

};

showNames();
// console.log(functionVar);
// console.log(functionConst);
// console.log(functionLet);
	// will result in errors as the vars/const/functions are declared inside a function


// NESTED Functions

function newFunction(){
	let name = "Jane";

	function nestedFunction(){
		let nestedName = "John";
		console.log(nestedName);
	}
	nestedFunction();
	console.log(name);
}
newFunction();
// nestedFunction();
		// returns an error, nestedFunction should be called inside the function where it was declared (newFunction)


/*
	miniactivity
		create a global-scope variable
		create a function
			create a let variable
			log in the console the two variables

		call the function	
*/

var globalVar2 = "My money don't jiggle jiggle";

function fold(){
	let localVar2 = "It folds"
	console.log(globalVar2);
	console.log(localVar2);
}

fold();


// using alert()
/*
	allows us to show a small window at the top of the browser to show information to the users
	As opposed to console.log, which only shows the message on the console.

	it allows us to show a short dialog or instructions to the user. The page will wait (continue to load) until the user dismisses the dialog.
*/

/*
alert("Hello World"); // will show as the page loads


// we can show an alert() to show a message to the user from a later function invocation
function showSampleAlert(){
	alert("Hello Again!");
};

showSampleAlert();
*/
// we will find that the page waits for the user to dismiss the dialog box before proceeding. we can witness this by reloading the page while the console is open.
console.log("I will be displayed after the alert has been closed.");


/*
	NOTES on using alert()
		shows only an alert() for short dialogs/messages to the user.
		do not overuse alert() as the programs needs to wait for the alert to end before continuing.
*/

// using prompt()
// shows a prompt on top of the window to gather input.

/*
	SYNTAX
		prompt("<dialogString>");
*/
// prompt will be returned as a string data type once the user dismisses the window.
/*
let samplePrompt = prompt("Enter your name: ");

console.log(typeof samplePrompt);
console.log("Hello " + samplePrompt);
*/

/*
let nullPrompt = prompt("Do not enter anything here:");

console.log(nullPrompt);
*/
// returns an empty spring if user selects "OK"
// returns NULL if user selects "Cancel"

/*
	NOTES using prompt()
		it can be used to gather user input and be used in our code. However, since it will have the page wait until the user finished or closed the window, it must not be overlooked.

		prompt() used globally will run immediately. for better usage and UX, it is better to use them in functions, and called only when needed.

*/


/*
	miniactivity
		create a function named welcomeMessage
			create 2 variables
				the firstName of the user that will come from a prompt
				the lastName of the user that will come from a prompt
	
		log in the console the message "Hello firstName lastName"
*/


function welcomeMessage(){
	let firstName = prompt("Please input your first name:")
	let lastName = prompt("Please input your last name:")

	console.log("Hello " + firstName + " " + lastName);
};

welcomeMessage();

// Function naming convention
	// function names should be definitive of the task that it will perform. it usually contains or starts with a verb/adjective

function getCourse(){
	let courses = ["Science 101", "Math 101", "English 101"];
	console.log(courses);
};
getCourse();

function get(){
	let name = "Jamie";
	console.log(name);
};
get();

function foo(){
	console.log(25%5);
}
foo();



