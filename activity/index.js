/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	function getUserDetails(){
		let firstName = prompt("Please enter your first name: ");
		let lastName = prompt("Please enter your last name: ");
		let userAge = prompt("Please enter you age: ");
		let userLocation = prompt("Where do you live? ");

		console.log("Hello " + firstName + " " + lastName);
		console.log("You are " + userAge + " years old");
		console.log("You live in " + userLocation);

	};

	getUserDetails();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function favBands(){
		let bandList = ["SNSD", "BlackPink", "Twice", "Aespa", "G-IDLE"];
		console.log("1. " + bandList[0]);
		console.log("2. " + bandList[1]);
		console.log("3. " + bandList[2]);
		console.log("4. " + bandList[3]);
		console.log("5. " + bandList[4]);
	}

	favBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function favMovies(){
		let movieList = ["Avengers:End Game", "Spiderman: Far From Home", "Doctor Strange", "Captain America: Civil War", "Spiderman: No Way Home"];

		let rotTomRatings = ["100%","90%","89%","90%","93%"];

		console.log("1. " + movieList[0]);
		console.log("Rotten Tomatoes Rating: " + rotTomRatings[0]);
		console.log("2. " + movieList[1]);
		console.log("Rotten Tomatoes Rating: " + rotTomRatings[1]);
		console.log("3. " + movieList[2]);
		console.log("Rotten Tomatoes Rating: " + rotTomRatings[2]);
		console.log("4. " + movieList[3]);
		console.log("Rotten Tomatoes Rating: " + rotTomRatings[3]);
		console.log("5. " + movieList[4]);
		console.log("Rotten Tomatoes Rating: " + rotTomRatings[4]);
	}

	favMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printFriends();


// console.log(friend1);
// console.log(friend2);